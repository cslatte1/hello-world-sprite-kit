//
//  AppDelegate.h
//  SpriteKitSimpleGame
//
//  Created by Ciaran Slattery on 2/14/17.
//  Copyright © 2017 RazewareLLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

